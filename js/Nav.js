/*
 Copyright 2017 Thomás Sousa Silva.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */

$.autoHideCollapseNavbar = function (selector = ".navbar-collapse") {
    $(selector).click(function (event) {
        var target = $(event.target);
        if (target.is("a:not(.dropdown-toggle)")) {
            target.closest(selector).collapse("hide");
        }
    });
};

$.fn.active = function (active = true, disableSiblings = true, parentSelector = "li") {
    var parent = $(this).closest(parentSelector);
    if (active) {
        parent.addClass("active");
        if (disableSiblings) {
            parent.siblings().removeClass("active");
        }
    } else {
        parent.removeClass("active");
    }
    return this;
};

$.fn.activeTab = function (active = true, disableSiblings = true, parentSelector = "li") {
    var tab = $(this);
    var parent = tab.closest(parentSelector);
    if (active) {
        parent.addClass("active");
        $(tab.attr("href")).addClass("active");
        if (disableSiblings) {
            parent.siblings().removeClass("active");
            parent.siblings().children("[data-toggle=tab]").each(function () {
                $($(this).attr("href")).removeClass("active");
            });
        }
    } else {
        parent.removeClass("active");
        $(tab.attr("href")).removeClass("active");
    }
    return this;
};